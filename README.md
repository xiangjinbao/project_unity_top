# GameAndUnity-TechLib

# 引擎技术尝试

- [Animancer-Pro] (https://assetstore.unity.com/packages/tools/animation/animancer-pro-116514) （基于Playable的简单强大的动画解决方案）
- [ProBuilder/UModeler] (https://assetstore.unity.com/packages/tools/modeling/umodeler-80868) （快速关卡原型构建解决方案）
- [FGUI] (https://github.com/fairygui/FairyGUI-unity) （简单强大的UI解决方案）
- [URP] (https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@7.5/manual/index.html) （Unity官方可编程渲染管线-通用渲染管线）
- [ShaderGraph/AmplifyShaderEditor] (https://assetstore.unity.com/packages/tools/visual-scripting/amplify-shader-editor-68570) （自定义Shader编辑器）
- [Bolt] (https://assetstore.unity.com/packages/tools/visual-scripting/bolt-163802) （可视化脚本）

---
# 资源管理方案
- [XAsset] (https://github.com/xasset/xasset) （简单强大的资源管理插件，基于引擎自身的依赖关系实现，建议在项目中采用该方案）
- [GameFramework] (https://github.com/EllanJiang/GameFramework) （框架中的Resource模块，提供了一个自己维护引用的高度可定义资源管理框架，非常值得学习研究）

---
# 常用的热更新方案
- [HybridCLR] (https://github.com/focus-creative-games/hybridclr) （基于C#的热更新方案，目前效率最高的热更方案，单程序域运行）
- [tolua] (https://github.com/topameng/tolua) (基于lua实现的嵌入虚拟机热更解决方案)
- [xlua] (https://github.com/Tencent/xLua) (基于lua实现的嵌入虚拟机热更解决方案，新加了动态修复C#函数功能)
- [ILRuntime] (https://github.com/Ourpalm/ILRuntime) (自己实现了一个IL运行时的解析器，以此来解决没有JIT环境下运行C#)

---
# 技能辅助开发工具
- [Slate] (https://assetstore.unity.com/packages/tools/animation/slate-cinematic-sequencer-56558) (类似Timeline的时间线编辑工具,UI效果较好)
- [Flux] (https://assetstore.unity.com/packages/tools/animation/flux-18440) (类似Timeline的时间线编辑工具，效果略差)
- [SuperCLine] (https://assetstore.unity.com/packages/tools/game-toolkits/cline-action-editor-2-163343)(仿照Flux实现的可直接在项目使用的技能编辑器)

---
# 编辑器扩展工具
- [Odin-Inspector] (https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041) （编辑器扩展、工作流改善）
- [NaughtyAttributes] (https://github.com/dbrizov/NaughtyAttributes) (实现基于属性的常用编辑器效果样式)
- [XiaoCaoTools] (https://github.com/smartgrass/XiaoCaoTools) (基于上一个插件扩展的Window常用属性编辑器)
- [XCSkillEditor]  (https://github.com/smartgrass/XCSkillEditor_Unity) (基于Lux开发的技能编辑器)
- [MDDSkillEngine] (https://gitee.com/mtdmt/MDDSkillEngine) (基于UnityTimeline实现的技能编辑器)

---
# 前端开发框架
- [ET] (https://github.com/egametang/ET) (基于C#实现的双端共享的客户端和服务器框架)
- [GameFramework] (https://github.com/EllanJiang/GameFramework) （通用涵盖多模块客户端游戏开发框架）
- [KSFramework] (https://github.com/mr-kelly/KSFramework) (通用的客户端框架)
- [BDFramework] (https://github.com/yimengfan/BDFramework.Core) (通用的客户端框架)
- [QFramework] (https://github.com/liangxiegame/QFramework) (通用的客户端框架)
- [MyUnityFrameWork] (https://github.com/GaoKaiHaHa/MyUnityFrameWork) (通用的客户端框架)
- [loxodon-framework] (https://github.com/vovgou/loxodon-framework) (通用的客户端框架)
- [TinaX] (https://github.com/yomunsam/TinaX) (通用的客户端框架)
- [ColaFrameWork] (https://github.com/XINCGer/ColaFrameWork) (通用的客户端框架)
- [RPGCore] (https://github.com/Fydar/RPGCore) (通用的客户端框架)
- [MotionFramework] (https://github.com/gmhevinci/MotionFramework) (通用的客户端框架)
- [HTFramework] (https://github.com/SaiTingHu/HTFramework) (通用的客户端框架)
- [HTFramework] (https://github.com/mofr/Diablerie) (通用的客户端框架)

---
# 后端开发框架
- [ET] (https://github.com/egametang/ET) (基于C#实现的双端共享的客户端和服务器框架)
- [nakama] (https://github.com/heroiclabs/nakama) (通用的服务器框架)
- [NoahGameFrame] (https://github.com/ketoo/NoahGameFrame) (通用的服务器框架)
- [GeekServer] (https://github.com/leeveel/GeekServer) (通用的服务器框架)
- [kbengine] (https://github.com/kbengine/kbengine) (通用的服务器框架)
- [leaf] (https://github.com/name5566/leaf) (通用的服务器框架)

---
# Actor框架
- https://github.com/akkadotnet/akka.net
- https://github.com/AsynkronIT/protoactor-dotnet
- https://github.com/microsoft/service-fabric-services-and-actors-dotnet

---
# DOTS面向数据技术栈
- https://github.com/Leopotam/ecs
- https://github.com/sebas77/Svelto.ECS
- https://github.com/PixeyeHQ/actors.unity
- https://github.com/EcsRx/ecsrx
- https://github.com/chromealex/ecs
- https://github.com/scellecs/Morpeh

---
# IoC
- https://github.com/strangeioc/strangeioc
- https://github.com/modesttree/Zenject
- https://github.com/CatLib/CatLib

---
# 战斗、技能系统
- https://github.com/m969/EGamePlay
- https://github.com/sjai013/UnityGameplayAbilitySystem
- https://github.com/tranek/GASDocumentation （虚幻引擎的GamePlay Ability System 文档）
- https://github.com/dongweiPeng/SkillSystem
- https://github.com/delmarle/RPG-Core
- https://github.com/KrazyL/SkillSystem-3 （Dota2 alike Skill System Implementation for KnightPhone）
- https://github.com/dx50075/SkillSystem
- https://github.com/michaelday008/AnyRPGAlphaCode
- https://github.com/weichx/AbilitySystem
- https://github.com/gucheng0712/CombatDesigner （A Frame Based Visual Combat System in Unity Game Engine.）
- https://github.com/PxGame/XMLib.AM

---
# 帧同步框架
- https://github.com/JiepengTan/LockstepEngine
- https://github.com/proepkes/UnityLockstep
- https://github.com/SnpM/LockstepFramework

---
# 常用工具插件
## 黑客工具、网络异常模拟
- https://github.com/Z4nzu/hackingtool

## 资源检查
- https://github.com/ZxIce/AssetCheck
- https://github.com/yasirkula/UnityAssetUsageDetector

## Unity小工具
- https://github.com/lujian101/UnityToolDist （动画压缩、矩阵调试等）
- https://github.com/Unity-Technologies/VFXToolbox
- https://github.com/Deadcows/MyBox
- https://github.com/Ayfel/PrefabLightmapping
- https://github.com/laurenth-personal/lightmap-switching-tool
- https://github.com/yasirkula/UnityRuntimeInspector

## 程序化工具
- https://github.com/Syomus/ProceduralToolkit
- https://github.com/mxgmn/WaveFunctionCollapse

---
# 图形渲染
## 水渲染
- https://github.com/flamacore/UnityHDRPSimpleWater

## 镜面反射
- https://github.com/Kink3d/kMirrors （URP）
- https://github.com/ColinLeung-NiloCat/UnityURP-MobileScreenSpacePlanarReflection

## 卡通渲染
- https://github.com/ColinLeung-NiloCat/UnityURPToonLitShaderExample
- https://github.com/unity3d-jp/UnityChanToonShaderVer2_Project
- https://github.com/Kink3d/kShading
- https://github.com/SnutiHQ/Toon-Shader
- https://github.com/IronWarrior/UnityToonShader
- https://github.com/Jason-Ma-233/JasonMaToonRenderPipeline
- https://github.com/ronja-tutorials/ShaderTutorials
- https://github.com/you-ri/LiliumToonGraph
- https://github.com/madumpa/URP_StylizedLitShader
- https://github.com/Sorumi/UnityToonShader
- https://github.com/ChiliMilk/URP_Toon

## 草渲染
- https://github.com/ColinLeung-NiloCat/UnityURP-MobileDrawMeshInstancedIndirectExample

## Decals
- https://github.com/Kink3d/kDecals

## 体素
- https://github.com/mattatz/unity-voxel

## 体积雾
- https://github.com/ArthurBrussee/Vapor

---
# 其他相关库
## 网络库
- https://github.com/RevenantX/LiteNetLib
- https://github.com/BeardedManStudios/ForgeNetworkingRemastered
- https://github.com/Yinmany/NetCode-FPS

## 序列化
- https://github.com/google/flatbuffers (据说序列化快，占内存大，相比于pb，适合游戏开发)
- https://github.com/jamescourtney/FlatSharp

## 动态表达式解析库
- https://github.com/davideicardi/DynamicExpresso
- https://github.com/zzzprojects/Eval-Expression.NET
- https://github.com/mparlak/Flee
- https://github.com/codingseb/ExpressionEvaluator
- http://wiki.unity3d.com/index.php/ExpressionParser

## 物理碰撞
- https://github.com/AndresTraks/BulletSharp
- https://github.com/Zonciu/Box2DSharp
- https://github.com/JiepengTan/LockstepCollision
- https://github.com/Prince-Ling/LogicPhysics
- https://github.com/aaa719717747/TrueSyncExample
- https://github.com/dotnet-ad/Humper

## 动态骨骼 
- https://github.com/OneYoungMean/Automatic-DynamicBone

## 图节点式编辑器（Graph Editor）
- https://github.com/alelievr/NodeGraphProcessor
- https://github.com/Siccity/xNode
- https://github.com/nicloay/Node-Inspector

## 行为树
- https://github.com/meniku/NPBehave
- https://github.com/ashblue/fluid-behavior-tree
- https://github.com/luis-l/BonsaiBehaviourTree

## 笔刷绘图
- https://github.com/EsProgram/InkPainter

## ScrollRect
- https://github.com/qiankanglai/LoopScrollRect

## SRP项目
- https://github.com/keijiro/TestbedHDRP

## 敏感词库
- https://github.com/toolgood/ToolGood.Words

## 算法
- https://github.com/labuladong/fucking-algorithm
- https://github.com/azl397985856/leetcode
- https://github.com/halfrost/LeetCode-Go

## 原生平台交互
- https://github.com/yasirkula/UnityNativeShare
- https://github.com/hellowod/u3d-plugins-development

## GPU蒙皮动画
- https://github.com/chenjd/Render-Crowd-Of-Animated-Characters

---
# 优秀引用库
- https://github.com/utilForever/game-developer-roadmap (游戏开发者路线图)
- https://github.com/RyanNielson/awesome-unity 
- https://github.com/MFatihMAR/Game-Networking-Resources
- https://github.com/Gforcex/OpenGraphic
- https://github.com/insthync/awesome-unity3d
- https://github.com/killop/anything_about_game
- https://github.com/uhub/awesome-c-sharp
